$(document).ready(function () {
    //https://codepen.io/sashatran/pen/mOWLLB

    let screenWidth = $(window).width();
    let widthOffset = 10;
    let snowCount = 500;
    let snowInterval = 250;
    let snowImage = ['❅', '❅', '❆', '❄', '❅', '❆', '❄', '❅', '❆', '❄'];

    //make snow
    snowDrop(snowCount, randomInt(screenWidth / 2 - 100, screenWidth / 2 + 100));
    snow(snowCount, snowInterval);

    function snow(num, snowInterval) {
        if (num > 0) {
            setTimeout(function () {
                $('#drop_' + randomInt(1, snowCount)).addClass('animate');
                num--;
                snow(num, snowInterval);
            }, snowInterval);
        }
    };

    function snowDrop(num, position) {
        if (num > 0) {
            var drop = '<div class="drop snow" id="drop_' + num + '"></div>';

            $('body').append(drop);
            $('#drop_' + num).css('left', position);
            // $('#drop_' + num).text(snowImage[randomInt(0, snowImage.length - 1)]);
            // $('#drop_' + num).css('font-size', randomInt(40, 60) + '%');
            num--;
            snowDrop(num, randomInt(widthOffset, screenWidth - widthOffset));
        }
    };

    function randomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
})