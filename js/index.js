$(document).ready(function () {
    const tabs = document.querySelectorAll('.tab-item');
    const classActive = 'active';
    const imgSources_tab1 = [
        'https://scontent.fhph1-2.fna.fbcdn.net/v/t1.0-9/52762450_1285249951625525_2111614537865101312_o.jpg?_nc_cat=103&ccb=2&_nc_sid=a4a2d7&_nc_ohc=r35QcYxEiEMAX_dsTF5&_nc_ht=scontent.fhph1-2.fna&oh=9eefdefe7503e77395b3b218d76d5e00&oe=5FFE85B1',
        'https://scontent.fhph1-1.fna.fbcdn.net/v/t1.0-9/16142674_765866163563909_3617407541816597180_n.jpg?_nc_cat=102&ccb=2&_nc_sid=a4a2d7&_nc_ohc=VV6EMCMOjaQAX-0Z5cb&_nc_ht=scontent.fhph1-1.fna&oh=af36c0e972a3ccf962b0738e9e597ede&oe=6000ABCC',
        'https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-9/52898516_1289944964489357_2891202916574560256_o.jpg?_nc_cat=108&ccb=2&_nc_sid=a4a2d7&_nc_ohc=DIwQDWvMAfoAX9fjKuS&_nc_oc=AQliIT-ygcM4-I3w3jGm0Cb0179ptMM9KavvWnvoIIrLHmj6L4jhmO56g-jh-8eq2es&_nc_ht=scontent-sin6-2.xx&oh=6221343c6d05fe5fe74b5a51aa912318&oe=5FFF25F7',
        'https://scontent.fhph1-1.fna.fbcdn.net/v/t1.0-9/130252681_1878785102272004_1079852184354183205_n.jpg?_nc_cat=110&ccb=2&_nc_sid=09cbfe&_nc_ohc=dTD8o9N4AqEAX8WqXUz&_nc_ht=scontent.fhph1-1.fna&oh=1c03c3674f78dbe9d5209d17244bd8fc&oe=600211A5',
        'https://scontent.fhph1-2.fna.fbcdn.net/v/t1.0-9/87601357_1609590845858099_7834433463628333056_o.jpg?_nc_cat=105&ccb=2&_nc_sid=174925&_nc_ohc=HHLIUmjeQRAAX-td2Pv&_nc_ht=scontent.fhph1-2.fna&oh=bc6f5c37a8f2b58223d555b7ab19ad62&oe=5FFF2A43',
        'https://scontent.fhph1-2.fna.fbcdn.net/v/t1.0-9/77072042_1521455768004941_3480730935077371904_n.jpg?_nc_cat=109&ccb=2&_nc_sid=174925&_nc_ohc=h1ic6ZB3U-kAX-gGsIV&_nc_ht=scontent.fhph1-2.fna&oh=0008efa6531097768d504d01259c46e8&oe=60011F81',
        'https://scontent.fhph1-1.fna.fbcdn.net/v/t1.0-9/74323263_1490626731087845_1555535960886738944_n.jpg?_nc_cat=104&ccb=2&_nc_sid=174925&_nc_ohc=p6uVn2B5RlUAX_2W-MW&_nc_ht=scontent.fhph1-1.fna&oh=31a129e9abc4046932e9d129262f15c9&oe=60008AA4',
        'https://scontent.fhph1-1.fna.fbcdn.net/v/t1.0-9/51435161_1273250879492099_7373341618864128000_n.jpg?_nc_cat=111&ccb=2&_nc_sid=174925&_nc_ohc=sd_VuNWMTroAX8Sde5w&_nc_ht=scontent.fhph1-1.fna&oh=8ac162a07fbd923dc285c2e4a77db84c&oe=6002D220',
        'https://scontent.fhph1-1.fna.fbcdn.net/v/t1.0-9/16114177_763491393801386_2165234623850750048_n.jpg?_nc_cat=104&ccb=2&_nc_sid=174925&_nc_ohc=Il7fEuoAC_MAX9YvctK&_nc_ht=scontent.fhph1-1.fna&oh=d791b9a94d3c2372f56f60e5091ec831&oe=6000A661',
    ];
    const imgSources_tab2 = [
        'https://scontent.fhph1-1.fna.fbcdn.net/v/t1.0-9/126168591_1859300160887165_3319194060338565885_o.jpg?_nc_cat=100&ccb=2&_nc_sid=8bfeb9&_nc_ohc=eb_jg0iWwU4AX_h5h6K&_nc_ht=scontent.fhph1-1.fna&oh=61bae381f7fdcea41510d401b7a1e4bc&oe=5FFF3D64',
        'https://scontent.fhph1-2.fna.fbcdn.net/v/t1.0-9/124495586_1852182794932235_7406396390924598243_o.jpg?_nc_cat=103&ccb=2&_nc_sid=8bfeb9&_nc_ohc=z70uiXhahJMAX_XRI-k&_nc_oc=AQl2MdKqmpjffN1s2-X39biB1YsZ72g9C1lOCupV5yu3N2-Lqlr6pDOLS2NuDuWcelE&_nc_ht=scontent.fhph1-2.fna&oh=d2241efcc499914f2cdb5046b00ea121&oe=5FFDF065',
        'https://scontent.fhph1-1.fna.fbcdn.net/v/t1.0-9/106898125_1733441006806415_4504739657075264557_o.jpg?_nc_cat=100&ccb=2&_nc_sid=0debeb&_nc_ohc=Sj1XK70w9TsAX9e-fXu&_nc_ht=scontent.fhph1-1.fna&oh=1cc7ff5b2c0592ec90876815844f144a&oe=5FFE24C2',
        'https://scontent.fhph1-1.fna.fbcdn.net/v/t1.0-9/15873514_753878261429366_5770444645311477737_n.jpg?_nc_cat=102&ccb=2&_nc_sid=174925&_nc_ohc=q6ksiBth9bwAX-S7ubM&_nc_ht=scontent.fhph1-1.fna&oh=d02fd28674a91cd69527be49d4c0ba40&oe=6000384F',
        'https://scontent.fhph1-2.fna.fbcdn.net/v/t1.0-9/15673059_744525392364653_5116675553634819403_n.jpg?_nc_cat=105&ccb=2&_nc_sid=174925&_nc_ohc=lrA67BY_o1cAX9knwRb&_nc_ht=scontent.fhph1-2.fna&oh=421980327e6e30ecb8ad4942fc520695&oe=5FFF9E47',
        'https://scontent.fhph1-2.fna.fbcdn.net/v/t1.0-9/15056303_717815765035616_6282552568412127158_n.jpg?_nc_cat=109&ccb=2&_nc_sid=174925&_nc_ohc=_cuvXg0ps6cAX_WJSlG&_nc_ht=scontent.fhph1-2.fna&oh=bdcf8972f293447e2bf2e5c24727b371&oe=60021FAA',
        'https://scontent.fhph1-2.fna.fbcdn.net/v/t1.0-9/17757461_808104756006716_3118713403380182266_n.jpg?_nc_cat=105&ccb=2&_nc_sid=8bfeb9&_nc_ohc=Tpqs07yJZqkAX9bmdHT&_nc_ht=scontent.fhph1-2.fna&oh=ba857295bdf9a0ddd7b6d12b96b4d371&oe=600237BD',
        'https://scontent.fhph1-1.fna.fbcdn.net/v/t1.0-9/16196008_766553410161851_430865022348328756_n.jpg?_nc_cat=100&ccb=2&_nc_sid=8bfeb9&_nc_ohc=1YiIQ5kiyEMAX_gyi-I&_nc_ht=scontent.fhph1-1.fna&oh=9b329a94349f89452eb340f635c13a23&oe=5FFF8C7D',
        'https://scontent.fhph1-2.fna.fbcdn.net/v/t1.0-9/16298542_766553013495224_4942167377459042662_n.jpg?_nc_cat=105&ccb=2&_nc_sid=8bfeb9&_nc_ohc=8HmWWINmMQAAX_8hId2&_nc_ht=scontent.fhph1-2.fna&oh=8d643f5104b4324fa060547465ce7f02&oe=6000C2EB',
    ];
    var messages_tab1 = [
        'Chào anh!',
        'Em yêu anh <3',
        'Chúc anh một ngày tốt lành!',
        'Chơi game với em không?',
        'Mèo méo meo!',
        'Thôi đi ngủ nha!',
        'Em có xinh không?',
        'Anh đi đâu đấy?',
        'Hát cùng em nào!',
        'Bye anh!',
        'Buồn quá!',
        'Anh không đi chơi hả?',
        'Nghe em hát nhé!',
        'Nghe em rap không?',
        'Chả có ai rủ đi chơi...',
        'Mình yêu nhau đi :))',
    ];
    var messages_tab2 = [
        'Tèo Tí Tởn',
        'Nguyễn Đình Sơn',
        'Muốn Lấy Vợ',
        'Andy',
        'livescore',
        'Valkery',
        'SM',
        '_',
        'Bơ',
        'LếtĐếnMònMông',
        '.k',
        'Only me',
        'Shuジ',
        'Nà Ní',
        'Pha Quo Hun',
        'JaoJan3Ja',
        'Shinn',
        'HoàngQuốcThăng',
        'Only',
        '<blank>',
        'Hiển Hiển',
        'Toàn Nè',
        '[ Talk ] ๖ۣۜDạo ๖ۣۜChơi๖',
        'Speculator',
        'Một Kiếp Người',
        'Hoài',
        '[ZET] Boss',
        '桃玉维',
        'Celeb',
        'JaeGeol',
        'mr.D',
        'Cần Trẩu',
        'ƊøƦaeɱøn',
        'Tiểu Vàng',
        'Ôlê',
        'Hai Lúa NhàQuê',
        '[C.Ⱥ.C]ĬทƬɛɛṦT',
        'hoàng',
        'EmCV',
        'Daniel Hoang',
        'IDC-...987',
        'N.T.A',
        'Hoàng Anh',
        'First Love16-07-1994(yến july)',
        'Ánh nắng',
        'sky',
        'FBI',
        'Prø丶๖ۣۜCua',
        'Ffang Ngố',
        'Devil Man',
        'Đức',
        'Thành Trần Rc',
        'Sơn Nguyễn Tha',
        '__Mr-6917__',
        'Nguyễn Trọng H',
        'Bạn Anh Teddy',
    ];

    let currentActiveImage_tab1;
    let currentActiveImage_tab2;
    let currentMessages = messages_tab1;
    let arrayIndex = 0;

    let flipBoxes_tab1 = [];
    let flipBoxes_tab2 = [];
    let images_tab2 = [];

    let turnCount = 3;

    generateImages();
    handleTabClick();
    flipImageOnClick();

    function handleTabClick() {
        for (let i = 0; i < tabs.length; i++) {
            tabs[i].addEventListener('click', function () {
                arrayIndex = 0;
                if (i === 0) {
                    currentMessages = shuffle(messages_tab1);
                } else {
                    currentMessages = shuffle(messages_tab2);
                }
            });
        };
    }

    function generateImages() {
        let flipbox = document.createElement('div');
        // flipbox.className = 'flip-box col-3';
        flipbox.className = 'flip-box';
        let flipboxInner = document.createElement('div');
        flipboxInner.className = 'flip-box-inner';
        let flipboxFront = document.createElement('div');
        flipboxFront.className = 'flip-box-front';
        let flipboxBack = document.createElement('div');
        flipboxBack.className = 'flip-box-back';
        let flipboxFrontImg = document.createElement('img');
        flipboxFrontImg.className = 'image-fit';
        let flipboxBackP = document.createElement('p');
        flipboxBackP.className = 'vertical-center'
        flipbox.appendChild(flipboxInner);
        flipboxInner.appendChild(flipboxFront);
        flipboxInner.appendChild(flipboxBack);
        flipboxFront.appendChild(flipboxFrontImg);
        flipboxBack.appendChild(flipboxBackP);

        // let arrayIndex = 0;
        messages_tab1 = shuffle(messages_tab1);
        imgSources_tab1.forEach(img => {
            let flipboxClone = flipbox.cloneNode(true);
            let flipboxImg = flipboxClone.childNodes[0].childNodes[0].childNodes[0];
            flipboxImg.setAttribute('src', img);
            $('#tab1').append(flipboxClone);
        })

        // arrayIndex = 0;
        messages_tab2 = shuffle(messages_tab2);
        imgSources_tab2.forEach(img => {
            let flipboxClone = flipbox.cloneNode(true);
            let flipboxImg = flipboxClone.childNodes[0].childNodes[0].childNodes[0];
            flipboxImg.setAttribute('src', img);
            $('#tab2').append(flipboxClone);
        })
    }

    function flipImageOnClick() {
        flipBoxes_tab1 = document.querySelectorAll('#tab1 .flip-box');
        for (let i = 0; i < flipBoxes_tab1.length; i++) {
            flipBoxes_tab1[i].addEventListener('click', function () {
                flipBoxes_tab1[i].classList.toggle(classActive);
                if (flipBoxes_tab1[i].className.includes('active')) {
                    let p = flipBoxes_tab1[i].childNodes[0].childNodes[1].childNodes[0];
                    p.innerHTML = currentMessages[getArrayIndex(currentMessages)];
                }
                if (currentActiveImage_tab1 != null && currentActiveImage_tab1 != flipBoxes_tab1[i]) {
                    currentActiveImage_tab1.classList.toggle(classActive);
                }
                if (flipBoxes_tab1[i].className.includes(classActive)) {
                    currentActiveImage_tab1 = flipBoxes_tab1[i];
                } else {
                    currentActiveImage_tab1 = null;
                }
            });
        }

        flipBoxes_tab2 = document.querySelectorAll('#tab2 .flip-box');
        images_tab2 = document.querySelectorAll('#tab2 .flip-box .flip-box-inner .flip-box-front img');
        for (let i = 0; i < flipBoxes_tab2.length; i++) {
            flipBoxes_tab2[i].addEventListener('click', function a() {
                flipBoxes_tab2[i].classList.toggle(classActive);
                if (flipBoxes_tab2[i].className.includes('active')) {
                    var p = flipBoxes_tab2[i].childNodes[0].childNodes[1].childNodes[0];
                    p.innerHTML = currentMessages[getArrayIndex(currentMessages)];
                }
                if (currentActiveImage_tab2 != null && currentActiveImage_tab2 != flipBoxes_tab2[i]) {
                    currentActiveImage_tab2.classList.toggle(classActive);
                }
                if (flipBoxes_tab2[i].className.includes(classActive)) {
                    currentActiveImage_tab2 = flipBoxes_tab2[i];
                    turnCount--;
                    $('#txtTurnCount').text(turnCount);
                } else {
                    currentActiveImage_tab2 = null;
                }
                if (turnCount <= 0) {
                    flipBoxes_tab2.forEach(el => {
                        el.classList.add("disabledbutton");
                    });
                    images_tab2.forEach(img => {
                        img.classList.add("disabledImage");
                    })
                }
            });
        }
    }

    $('#btnReset').on('click', function () {
        flipBoxes_tab2.forEach(el => {
            el.classList.remove("disabledbutton");
        });
        images_tab2.forEach(el => {
            el.classList.remove("disabledImage");
        });
        currentActiveImage_tab2.classList.remove(classActive);
        currentActiveImage_tab2 = null;
        turnCount = 3;
        $('#txtTurnCount').text(turnCount);
    })

    function getRandom(array) {
        return array[Math.floor((Math.random() * array.length))];
    }

    function shuffle(arr) {
        for (let i = arr.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [arr[i], arr[j]] = [arr[j], arr[i]];
        }
        return arr;
    }
    function getArrayIndex(arr) {
        arrayIndex++;
        if (arrayIndex >= arr.length) {
            arrayIndex = 0;
        }
        return arrayIndex;
    }
})